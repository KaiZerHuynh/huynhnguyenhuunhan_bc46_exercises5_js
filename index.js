var numArr = []
function themSo(){
    var number = document.querySelector("#number").value*1;
    //thêm giá trị mảng
    numArr.push(number);
    //duyệt mảng
    var contenHTML = `<h3>Mảng gồm các phần tử là: ${numArr}</h3>`
    document.querySelector("#result").innerHTML = contenHTML;
}

function tinhTong(){
    var tongDuong = 0;
    for(var index = 0; index < numArr.length;index++)
    {
        if(numArr[index] > 0)
        {
            tongDuong += numArr[index];
        }
    }
    var contenHTML = `<h3>Tổng số dương là: ${tongDuong}</h3>`
    document.querySelector("#result1").innerHTML = contenHTML;
}

function demDuong(){
    var count = 0;
    for(var index = 0; index < numArr.length;index++)
    {
        if(numArr[index] > 0)
        {
            count ++;
        }
    }
    var contenHTML = `<h3>Tổng số dương trong mảng là: ${count}</h3>`
    document.querySelector("#result2").innerHTML = contenHTML;
}

function minArray(){
    var min = numArr[0];
    for(var index = 0; index < numArr.length;index++)
    {
        if(numArr[index] < min)
        {
            min = numArr[index];
        }
    }
    var contenHTML = `<h3>Số nhỏ nhất trong mảng là: ${min}</h3>`
    document.querySelector("#result3").innerHTML = contenHTML;
}

function minDuong(){
    var min = numArr[0];
    for(var index = 0; index < numArr.length;index++)
    {
        if(numArr[index] < min && numArr[index] > 0)
        {
            min = numArr[index];
        }
    }
    var contenHTML = `<h3>Số dương nhỏ nhất trong mảng là: ${min}</h3>`
    document.querySelector("#result4").innerHTML = contenHTML;
}

function lastOld(){
    var old = -1;
    for(var index = 0; index < numArr.length;index++)
    {
        if(numArr[index] % 2 == 0)
        {
            old = numArr[index];
        }
    }
    var contenHTML = `<h3>Số chẵn cuối cùng trong mảng là: ${old}</h3>`
    document.querySelector("#result5").innerHTML = contenHTML;
}

function swap(){
    var position1 = document.getElementById("position1").value*1;
    var position2 = document.getElementById("position2").value*1;
    var temp = numArr[position1];
    numArr[position1] = numArr[position2];
    numArr[position2] = temp;
    var contenHTML = `<h3>Kết quả sau khi đổi chỗ: ${numArr}</h3>`
    document.querySelector("#result6").innerHTML = contenHTML;
}


function sort(){
    for(var i = 0; i < numArr.length;i++)
    {
        for (var j = 0; j < numArr.length - 1; j++) {
            if (numArr[j] > numArr[j + 1]) {
                var temp = numArr[j];
                numArr[j] = numArr[j + 1];
                numArr[j + 1] = temp;
            }
        }
    }
    document.getElementById("result7").innerHTML = `<h3>Mảng mới là: ${numArr}</h3>`;
}

function isPrime(num) {
  if (num <= 1) {
    return false;
  }
  for (var i = 2; i <= Math.sqrt(num); i++) {
    if (num % i == 0) {
      return false;
    }
  }
  return true;
}

function findFirstPrime() {
    var soNguyenTo = 0;
    for (var i = 0; i < numArr.length; i++) {
        if (isPrime(numArr[i]) == true) {
            soNguyenTo = numArr[i];
            break;
        }
        else{
            soNguyenTo = -1;
        }
    }
  
  var contenHTML = `<h3>Số nguyên tố đầu tiên là: ${soNguyenTo}</h3>`
    document.querySelector("#result8").innerHTML = contenHTML;
}

function timSoNguyen()
{
    var count = 0;
    for (let i = 0; i < numArr.length; i++) {
        if (Number.isInteger(numArr[i])) {
          count++;
        }
    }
    var contenHTML = `<h3>Tổng số nguyên là: ${count}</h3>`
    document.querySelector("#result9").innerHTML = contenHTML;
}

function soSanh()
{
    var demDuong = 0;
    var demAm = 0;
    for(var index = 0; index < numArr.length; index++)
    {
        if(numArr[index] > 0)
        {
            demDuong++;
        }
        if(numArr[index] < 0)
        {
            demAm++;
        }
    }
    if(demDuong > demAm)
    {
        var contenHTML = `<h3>Số dương > Số âm</h3>`
        document.querySelector("#result10").innerHTML = contenHTML;
    }
    else{
        var contenHTML = `<h3>Số dương < Số âm</h3>`
        document.querySelector("#result10").innerHTML = contenHTML;
    }
}